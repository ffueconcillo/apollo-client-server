import { RESTDataSource, RequestOptions } from 'apollo-datasource-rest';

import { appConfig } from '../appConfig';

const {
  COUCHDB_HOST,
  COUCHDB_USERNAME,
  COUCHDB_PASSWORD,
  COUCHDB_PORT,
  COUCHDB_PROTOCOL,
} = process.env;
export class CouchDBRest extends RESTDataSource {
  constructor() {
    super(); 
    this.baseURL = 
      `${COUCHDB_PROTOCOL}://${COUCHDB_HOST}:${COUCHDB_PORT}/${appConfig.databaseName}/`;
  }

  willSendRequest(request: RequestOptions) {
    let auth:string = 'Basic ' + new Buffer(COUCHDB_USERNAME + ':' + COUCHDB_PASSWORD).toString('base64');
    request.headers.set('Authorization', auth);
  }

  async getUsers(limit = 10) {
    const data = await this.get('_design/users/_view/id', {
      per_page: limit,
      order_by: 'name',
    });
    return Array.isArray(data.rows) 
      ? data.rows.map((row:any) => row.value)
      : [];
  }

}