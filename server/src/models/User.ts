import * as Nano  from 'nano';
import { v4 as uuidv4 } from 'uuid';

import { database } from '../dbConnect';

interface Resource extends Nano.MaybeDocument {
  _id: string,
  _rev?: string
  doc_type: string
}

export class User implements Resource {
  _id: string
  _rev?: string
  doc_type: string
  name: string
  dob: string

  constructor(name: string, dob: string) {
    this._id = uuidv4();
    this._rev = undefined;
    this.doc_type = 'User';
    this.name = name;
    this.dob = dob;
  }

  processAPIResponse(response: Nano.DocumentInsertResponse) {
    if (response.ok === true) {
      this._id = response.id
      this._rev = response.rev
    }
  }

  async save() {
    let result = await database.insert(this)
      .then((response: Nano.DocumentInsertResponse) => {
        this.processAPIResponse(response);
        return this;
      })
      .catch(error => error.reason);

    return result;
  }
}