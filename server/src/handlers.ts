import { User } from './models/User';
import * as Hapi from "hapi";

export const createUsersHandler = async(request: Hapi.Request, h: Hapi.ResponseToolkit) => {
  let results = [];
  let newUser = new User('Bob', '2015-02-04');
  let res = await newUser.save()
  results.push(res);
  
  newUser = new User('Jane', '2015-02-04');
  res = await newUser.save()
  results.push(res);

  return results;
};