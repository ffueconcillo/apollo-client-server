import { createUsersHandler } from './handlers';

export const routes = [
  {
    method: 'GET',
    path: '/create-users',
    handler: createUsersHandler
  },
  // Add more routes here
];