import Nano from "nano";
import { appConfig } from './appConfig';

const {
  COUCHDB_HOST,
  COUCHDB_USERNAME,
  COUCHDB_PASSWORD,
  COUCHDB_PORT,
  COUCHDB_PROTOCOL,
} = process.env;

const DB_URL = 
  `${COUCHDB_PROTOCOL}://${COUCHDB_USERNAME}:${COUCHDB_PASSWORD}@${COUCHDB_HOST}:${COUCHDB_PORT}`

const connection = Nano(DB_URL);

export const database = connection.db.use(appConfig.databaseName);