import { ApolloServer } from 'apollo-server-hapi';
import { Server as HapiServer } from 'hapi';
import { RedisCache } from 'apollo-server-cache-redis';

import { appConfig } from './appConfig';
import { typeDefs } from './typeDefs';
import { resolvers } from './resolvers';
import { routes } from './routes';

import { CouchDBRest } from './datasources/CouchDBRest';
import { truncateSync } from 'fs';

async function StartServer() {

  const server = new ApolloServer({ 
    typeDefs, 
    resolvers, 
    dataSources: () => ({
      couchdbAPI: new CouchDBRest(),
    }), 
    cache: new RedisCache({
      host: 'localhost',
      // Options are passed through to the Redis client
    }),
    // context,  # user information per request
  });

  const app = new HapiServer(appConfig.hapi);

  app.route(routes);

  await server.applyMiddleware({
    app,
    cors: { origin: [ '*' ] },
  });

  await server.installSubscriptionHandlers(app.listener);

  await app.start();
  
  console.log('Server running on %s', app.info.uri);

}

StartServer().catch(error => console.log(error));