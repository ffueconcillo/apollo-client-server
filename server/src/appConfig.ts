export const appConfig = {
  hapi: {
    host: process.env.SERVICE_HOST || 'localhost',
    port: process.env.EXPOSED_HOST || 9000,
  },
  databaseName: 'mydb'
};
