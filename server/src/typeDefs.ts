import { gql } from 'apollo-server-hapi';

// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
export const typeDefs = gql`
  type User {
    name: String
    dob: String
  }

  type Query {
    Users: [User]
  }
`;
