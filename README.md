# Apollo Client + Server using React and CouchDB

A boilerplate for Apollo Client and Server using TypeScript, React and CouchDB

## Technologies

### Frontend:
- ReactJS
- Material UI
- Apollo Client
- TypeScript

### Backend:
- Apollo Server & HapiJS
- [Dockerized CouchDB](https://github.com/bitnami/bitnami-docker-couchdb)
- [CouchDB Nano](https://github.com/apache/couchdb-nano)
- Redis for Caching

## Local Development Setup

### Backend
- Clone this repository
- Install dependencies
```
$ cd ./server
$ npm install
```
- Run server application
```
$ source ./env
$ npm start
```

- Run dockerized CouchDB and Redis
```
$ docker stack deply -c stack.yaml couchdb
```
- Check CouchDB is running at http://127.0.0.1:5984/_utils/#login. Default username: admin, password: couchdb
- Create a non-partitioned database: mydb.  Database name is set in server/appConfig.js > databaseName

- Install dependencies
```
$ cd ./server
$ npm install
```
- Run server application
```
$ npm start
```

## Frontend
- Install dependencies
```
$ cd ./client
$ npm install
```
- Run server application
```
$ npm start
```






