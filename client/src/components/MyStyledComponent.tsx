import * as React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing(2)
    },
    title: {
      borderBottom: `2px solid ${theme.palette.primary.main}`
    }
  })
);

type Props = {
  title?: string;
};

export const MyStyledComponent = ({ title }: Props) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <h4 className={classes.title}>
        {title || 'My Styled Component'}
      </h4>
    </div>
  );
}